﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;
            Console.Write("a= ");
            a = int.Parse(Console.ReadLine());
            Console.Write("b= ");
            b = int.Parse(Console.ReadLine());

            c = MyLibrary.MyMath.add(a, b);
            Console.WriteLine($"{a} + {b} = {c}");
            Console.ReadLine();
        }
    }
}
