﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Lab1_SE1421_LeDacHoangPhuc_SE140036
{
    class Book
    {
        private string code;
        private string name;
        private string author;
        private string publisher;

        public Book()
        {
        }

        public Book(string codeInput, string nameInput, string authorInput, string publisherInput)
        {
            code = codeInput;
            name = nameInput;
            author = authorInput;
            publisher = publisherInput;
        }

        public string getCode()
        {
            return code;
        }

        public void setCode(string code)
        {
            this.code = code;
        }

        public string getName()
        {
            return name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public string getAuthor()
        {
            return author;
        }

        public void setAuthor(string author)
        {
            this.author = author;
        }

        public string getPublisher()
        {
            return publisher;
        }

        public void setPublisher(string publisher)
        {
            this.publisher = publisher;
        }

        public string printString()
        {
            return code + ", " + name + ", " + author + ", " + publisher;
        }

    }
}
