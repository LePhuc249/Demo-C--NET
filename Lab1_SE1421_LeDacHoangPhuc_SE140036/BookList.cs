﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Lab1_SE1421_LeDacHoangPhuc_SE140036
{
    class BookList
    {
        private Book[] list = null; // List chứa các Book
        private int count = 0;

        public BookList(int size) // Contructor
        {
            if (size < 100)
            {
                size = 100;
            }
            list = new Book[size];
        }

        public int findBookExistByCode(string aCode) // Hàm tìm Book theo code để tìm ra Book code đã có 
        {
            for (int i = 0; i < count; i++)
            {
                if (String.Compare(aCode, list[i].getCode()) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public void addBook() // Hàm tạo ra một book mới
        {
            if (count == list.Length)
            {
                WriteLine("List is full!");
            }
            else
            {
                string newCode, newName, newAuthor, newPublisher;
                int pos = 0;
                do
                {
                    WriteLine("Enter the Book code: ");
                    newCode = ReadLine();
                    try
                    {
                        int value = int.Parse(newCode);
                        if (value <= 0 || value > 999)
                        {
                            WriteLine("Invalid Value (<= 0, > 999) ");
                        }
                        else
                        {
                            newCode = String.Format("B{0}", value);
                        }
                    }
                    catch (Exception e)
                    {
                        WriteLine(e.Message);
                        return;
                    }
                    pos = findBookExistByCode(newCode);
                    if (pos >= 0)
                    {
                        WriteLine(" This code is existed! ");

                    }
                } while (pos >= 0);
                WriteLine("Enter the Book name: ");
                newName = ReadLine().ToUpper();
                WriteLine("Enter the Book author: ");
                newAuthor = ReadLine();
                WriteLine("Enter the Book publisher: ");
                newPublisher = ReadLine();
                list[count++] = new Book(newCode, newName, newAuthor, newPublisher);
                WriteLine("The Book have been addded. ");

            }
        }

        public void searchByName() // Hàm tìm kiếm book theo name
        {
            String aName;
            if (count == 0)
            {
                WriteLine("Empty list");
                return;
            }
            WriteLine("Enter the name of Book you want to find:");
            aName = ReadLine().ToUpper();
            for (int i = 0; i < count; i++)
            {
                if (String.Compare(aName, list[i].getName()) == 0)
                {
                    WriteLine(list[i].printString());
                }
            }
        }

        public void print() // Hàm in ra danh sách book
        {
            if (count == 0)
            {
                WriteLine("Empty list.");
                return;
            }
            WriteLine("List of Book: ");
            for (int i = 0; i < count; i++)
            {
                WriteLine(list[i].printString());
            }
        }

        public void findName(String aName) // Hàm tìm book theo name
        {
            int tempNumber = count;
            for (int i = 0; i < count; i++)
            {
                if (String.Compare(aName, list[i].getName()) == 0)
                {
                    Book temp = list[count - 1];
                    list[count - 1] = list[i];
                    list[i] = temp;
                    count--;
                }

            }
            if (tempNumber == count)
            {
                WriteLine("This name doesnot exist");
            }
        }

        public void remove() // Hàm remove book ra khỏi list
        {
            if (count == 0)
            {
                WriteLine("Empty list.");
                return;
            }
            String removedName;
            WriteLine("Enter the name of remove Book:");
            removedName = ReadLine().ToUpper();
            findName(removedName);
            WriteLine("The Book " + removedName + " has been removed");
        }
    }

}
