﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Lab1_SE1421_LeDacHoangPhuc_SE140036
{
    class Menu
    {
        private string[] hint;
        private int temp = 0;

        public Menu(int sizeOfMenu)
        {
            if (sizeOfMenu < 1)
            {
                sizeOfMenu = 10;
            }
            hint = new string[sizeOfMenu];
        }

        public void addMenu(string newHint)
        {
            if (temp < hint.Length)
            {
                hint[temp++] = newHint;
            }
        }

        public int getChoice()
        {
            int result = 0;
            if (temp > 0)
            {
                for (int i = 0; i < temp; i++)
                {
                    WriteLine((i + 1) + "-" + hint[i]);
                }
                WriteLine("Input Command: ");
                try
                {
                    result = int.Parse(ReadLine());
                }
                catch (FormatException ex)
                {
                    WriteLine(ex.Message);
                    WriteLine("Press 'Enter' to exit program");
                    ReadLine();
                }
            }
            return result;
        }
    }
}
