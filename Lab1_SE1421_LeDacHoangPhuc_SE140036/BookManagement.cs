﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Lab1_SE1421_LeDacHoangPhuc_SE140036
{
    class BookManagement
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu(5);

            menu.addMenu("Add a book");
            menu.addMenu("Find a book");
            menu.addMenu("Show book list");
            menu.addMenu("Remove a book");
            menu.addMenu("Exit");

            int choice;
            BookList list = new BookList(50);
            do
            {
                WriteLine("****** Book Management ******");
                choice = menu.getChoice();
                switch (choice)
                {
                    case 1:
                        list.addBook();
                        break;
                    case 2:
                        list.searchByName();
                        break;
                    case 3:
                        list.print();
                        break;
                    case 4:
                        list.remove();
                        break;
                    case 5: break;
                }
            } while (choice >= 1 && choice < 5);
        }
    }
}
