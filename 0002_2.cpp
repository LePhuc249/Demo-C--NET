#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>

void ConvertToWords (char* num){
	int len = strlen(num);

	if (len == 0)
	{
		printf("Empty string\n");
		return;
	}
	if (len > 4)
	{
		printf("Length more than 4 is not supported\n");
		return;
	}

	//single digits
	char* SingleDigits[] = {"zero", "one", "two", "three", "four", "five", 
							"six", "seven", "eight", "nine"};

	//two digits
	char* TwoDigits[] = {"", "ten", "eleven", "twelve", "thirteen", "fourteen",
							"fifteen", "sixteen", "seventeen", "eighteen",
							"nineteen"};

	//ten mutiple
	char* TenMutiple[] = {"", "", "twenty", "thirty", "forty", "fifty",
								"sixty", "seventy", "eighty", "ninety"};

	//ten power
	char* TenPower[] = {"hundred", "thousand"};
	
	printf("The converted string: ");

	//single number
	if (len == 1){
		printf("%s\n", SingleDigits[*num - '0']);
		return;
	}
	while (*num != '/0'){
		if (len >= 3){
			if (*num - '0' != 0){
				printf("%s ", SingleDigits[*num - '0']);
				printf("%s ", TenPower[len - 3]);
			}
			--len;
		}
		
		else{
			if (*num == '1'){
				int sum = *num - '0' + *(num + 1) - '0';
				printf("%s\n", TwoDigits[sum]);
				return;
			}
			else if (*num == '2' && *(num + 1) == '0'){
				printf("twenty\n");
				return;
			}
			
			else {
				int i = *num - '0';
				printf("%s ", TenMutiple[i]);
				++num;
				if (*num != '0')
				{
					printf("%s ", SingleDigits[*num - '0']);
				}
				return ;
			}
		}
		++num;
	}
	
				
			
		
		
	
}

	int main(int argc, char const *argv[])
	{
		char num[10000];
		char key;
		do{
			printf("Input number: ");
			gets(num);
			ConvertToWords(num);
			fflush(stdin);
			printf("\nPress enter to another reverse, ESC to exit\n");
			key=getch();
		}while (key != 27);
		return 0;
	}
