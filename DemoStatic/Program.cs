﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoStatic
{
    class Program
    {
        public static int myVariable = 0;

        static Program()
        {
            myVariable = 100;
        }

        static void Main(string[] args)
        {
            new Program();
            Console.WriteLine(Program.myVariable);
            Program.myVariable += 10;
            new Program();
            Console.WriteLine(Program.myVariable);
            Console.ReadLine();
        }
    }
}
