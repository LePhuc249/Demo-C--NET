﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyDemo
{
    class Program
    {
        private int empID;
        private float curPay;
        private string fullname;

        public int MyProperty
        {
            get { return empID; }
            set { empID = value; }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        static void Main(string[] args)
        {
            Program p = new Program() { empID = 81, fullname = "Hoang Phuc" };// Ton tai tu .NET 3.5 tro len
            //p.empID = 81;
            Console.WriteLine("Employee ID: " + p.empID + " Employee name: " + p.fullname);
            Console.ReadLine();
        }
    }
}
