﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamWriterAndStreamReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("******Fun with Stream Writer/ Stream Reader******\n");

            StreamWriter writer = new StreamWriter("remainders.txt");
            writer.WriteLine("Don't forget Mother's Day this year...");
            writer.WriteLine("Don't forget Father's Day this year...");
            writer.WriteLine("Don't forget these number");
            for(int i = 0; i < 10; i++)
            {
                writer.Write(i + " ");
            }
            writer.Write(writer.NewLine);
            writer.Close();
            Console.WriteLine("Created file and wrote some thoughts...");
            Console.WriteLine("Here are your thoughts:\n");
            StreamReader sr = new StreamReader("remainders.txt");
            string input = null;
            while((input = sr.ReadLine())!= null)
            {
                Console.WriteLine(input);
            }
            Console.ReadLine();
        }
    }
}
