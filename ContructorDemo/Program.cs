﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ContructorDemo
{
    class Product
    {
        private string name;
        public Product() => name = "new product"; // Ton tai o ban 6.0 tro len

        public Product(string name) => this.name = name;

        public string ProductName
        {
            get => name;
            set => name = value;
        }

        public virtual void print() => WriteLine($"Productname:{name}");
    }

    class Coffee: Product
    {
        private int Id;

        public Coffee(int id, string name) : base(name) => Id = id;

        //public new void print() => WriteLine($"id:{Id}," + $"name:{ProductName}");
        //public override void print() => WriteLine($"id:{Id}," + $"name:{ProductName}");
        public virtual void print() => WriteLine($"id:{Id}," + $"name:{ProductName}");
    }

    class Program{

        static void Main(string[] args)
        {
            Coffee coffee = new Coffee(1, "Cafe Viet");
            coffee.print();
            Product product = coffee;
            product.print();
            Console.ReadLine();
        }

    }
}
