﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelgateKiemHHDemo
{
    public delegate int MyDele(int x);
    class Program
    {
        static int Square(int x) => x * x;
        static void Main(string[] args)
        {
            int r;
            MyDele dele = new MyDele(Square);
            r = dele(3);
            Console.WriteLine(r);
            //
            MyDele my = Square;
            Console.WriteLine(my.Invoke(4));
            // Anonymous method
            MyDele m1 = delegate (int x)
            {
                return x = x;
            };
            Console.WriteLine(m1(5));
            // From .NET 3.5 
            MyDele my2 = ((x) => x*x);
            Console.WriteLine(my2(6));
            // Su dung function delegate
            Func<int, int> my3 = (x => x * x);
            Console.WriteLine(my3(7));

            Func<double, double, string> f = ((a, b) =>
            {
                return (a + b).ToString();
            });
            Console.WriteLine(f(3.4, 4.5));
            Console.ReadLine();

        }
    }
}
