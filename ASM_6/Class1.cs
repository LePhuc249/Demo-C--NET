﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASM_6
{
    class ClassMedicine
    {
        private string code;
        private string name;
        private string manufacture;
        private int price;
        private int quantity;
        private string date;
        private string expireDate;
        private int batchNumber;

        public Medicine()
        {
            code = null;
            name = null;
            manufacture = null;
            price = 0;
            quantity = 0;
            date = null;
            expireDate = null;
            batchNumber = 0;
        } 
        
        public Medicine(string code, string name, string manufacture, int price, int quantity, string date, string expireDate, int batchNumber)
        {
            this.code = code;
            this.name = name;
            this.manufacture = manufacture;
            this.price = price;
            this.quantity = quantity;
            this.date = date;
            this.expireDate = expireDate;
            this.batchNumber = batchNumber;
        }
    }
}
