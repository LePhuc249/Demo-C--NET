﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ASM_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            uint age;
            String gender;
            DateTime birthday;
            string address;
            string email;
            string fatherName;
            string motherName;

            WriteLine("Enter information : ");
            WriteLine("----------------------***----------------------");
            WriteLine("Enter student name: ");
            name = ReadLine();
            WriteLine("Enter student gender: ");
            gender = ReadLine();
            WriteLine("Enter student age: ");
            age = uint.Parse(ReadLine());
            WriteLine("Enter student BirthDay(MM/DD/YYYY): ");
            birthday = DateTime.Parse(ReadLine());
            WriteLine("Enter student address: ");
            address = ReadLine();
            WriteLine("Enter student email: ");
            email = ReadLine();
            WriteLine("Enter father's name: ");
            fatherName = ReadLine();
            WriteLine("Enter mother's name: ");
            motherName = ReadLine();

            //Xuat cac thong tin ra man hinh
            WriteLine("*** Student information: ***\n");
            WriteLine("Student name: " + name);
            WriteLine("Student age: " + age);
            WriteLine("Student gender: " + gender);
            WriteLine("Student birthday: " + birthday);
            WriteLine("Student address: " + address);
            WriteLine("Student email: " + email);
            WriteLine("Student father's name: " + fatherName);
            WriteLine("Student mother's name: " + motherName);
            ReadLine();
        }
    }
}
