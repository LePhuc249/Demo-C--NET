﻿using System;
using abc =  System.Collections.Generic; // abc là một cái tên mới, một cái dùng để gọi tắt hoặc trong trường hợp bị 
// trùng namespace. Đã tồn tại ở phiên bản 1.0-2.0. Có thể dùng khi gói thư viện có tên quá dài
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;// Chỉ tồn tại ở bản 6.0 trở lên

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Hello world!");
            ReadLine();
            //abc.List<>;
        }
    }
}
