﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalendaDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime d = monthCalendar1.SelectionStart;
            string dateStr = string.Format("{0}/{1}/{2}/{3}", d.DayOfWeek,
                d.Day, d.Month, d.Year);

            MessageBox.Show("Here is your day: " + dateStr);
        }
    }
}
