﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 3, y = 2;
            int r = Utils.Add(x, y);
            Console.WriteLine("{0}+{1}={2}",x,y,r);
            r = x.Sub(y); // gọi thông qua bên biến tiện hơn
            Console.WriteLine($"{x}-{y}={r}");
            Console.ReadLine();
        }
    }

    static class Utils
    {
        public static int Add(int a, int b)
        {
            return a + b; // Ra đời 1999
        }

        public static int Sub(this int a, int b) => a - b;
        // C# 6.0 ms tồn tại biểu thức "=>" function body is expression
        // chỉ sử dụng khi chỉ có 1 lệnh/ Biểu thức chỉ có 1 dòng
        // "=>" tồn tại ở Java 8 trở lên 
        // this tồn tại ở bản C# 3.5 trở lên, Microsoft cung cấp từ năm 2008
        // Muốn dùng thì chỉ dùng this ở đầu tiên k được đặt ở phía sau
    }
}
