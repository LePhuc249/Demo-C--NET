﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelgateDemo
{
    public delegate int BinaryOps(int x, int y);

    public class SimpleMath
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        public int Subtract(int x, int y)
        {
            return x - y;
        }

        public static int SquareNumber(int x)
        {
            return x * x;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("********** Delgate Example***********");
            SimpleMath m = new SimpleMath();
            BinaryOps b = new BinaryOps(m.Add);
            DisplayDelgateInfo(b);
            int i = b(10, 10);
            Console.WriteLine("\n10+10 is {0}", i);
            Console.ReadLine();
        }

        static void DisplayDelgateInfo(Delegate delgateObject)
        {
            foreach(Delegate d in delgateObject.GetInvocationList())
            {
                Console.WriteLine("Method name: {0}", d.Method);
                Console.WriteLine("Target name: {0}", d.Target);
            }
        }
    }

}
