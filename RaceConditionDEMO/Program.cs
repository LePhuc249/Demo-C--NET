﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Remoting.Contexts;

namespace RaceConditionDEMO
{

        [Synchronization]
        public class Printer : ContextBoundObject
        {
        public void printNumber()
        {
            //lock (this)
            //{
            Console.WriteLine("-> {0} is executing PrintNumber()", Thread.CurrentThread.Name);
            Console.Write("Your numbers: ");
            for (int i = 0; i < 10; i++)
            {
                Random r = new Random();
                Thread.Sleep(2000 * r.Next(5));
                Console.Write(i + ", ");
            }
            Console.WriteLine();
            //}
        }



        static void Main(string[] args)
        {
            Console.WriteLine("***** Synchronizing Threads *****");

            Printer p = new Printer();

            Thread[] threads = new Thread[10];
            for(int i = 0; i < 10; i++)
            {
                threads[i] = new Thread(new ThreadStart(p.printNumber));
                threads[i].Name = string.Format("Worked Thread #{0}", i);
            }
            foreach(Thread t in threads)
            {
                t.Start();
            }
            Console.ReadLine();
        }
    }
}
