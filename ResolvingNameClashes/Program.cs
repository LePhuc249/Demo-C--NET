﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResolvingNameClashes
{
    interface IDrawObject
    {
        void Draw();
    }

    interface IDrawShape
    {
        void Draw();
    }

    class MyGraphic: IDrawObject, IDrawShape
    {
        void IDrawObject.Draw() => Console.WriteLine("Draw a cat");
        void IDrawShape.Draw() => Console.WriteLine("Draw a circle");
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyGraphic graphic = new MyGraphic();
            IDrawObject drawObject = graphic;
            drawObject.Draw();
            IDrawShape drawShape = graphic;
            drawShape.Draw();
            Console.ReadLine();
        }
    }
}
