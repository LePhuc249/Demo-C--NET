﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefDemo
{   
    class Program
    {
        static void Calc(ref int a, int b, out int c)
        {
            //c+=b //Error exception do c k có giá trị nên làm sao lấy để mà thay đổi được
            a += b; // OK
            c = a + b; // OK, không có dòng này sẽ lỗi do là k thấy thằng c nào để modify
            b = a + c;
        }

        static void SumArray(out int s, params int[] arr) => s = arr.Sum();

        static void Main(string[] args)
        {
            int x = 3, y = 2, z = 1, s;
            Calc(ref x, y, out z);
            // ref: x sẽ truyền đi cả giá trị và địa chỉ => 1 vùng nhớ có 2 biến (a và x ) cùng tham chiếu 
            // Cho nên phải khởi tạo giá trị cho x. Nếu ở hàm k có hàm nào thay đổi giá trị biến a thì nó chấp nhận hết
            // Chỉ có y là gọi hàm k => y k thay đổi => nếu câu nào đáp án giống số đầu vào thì chắc chắn nó đúng
            // out: z truyền cho c. Truyền theo out thì mang tính chất bắt buộc nghĩa là truyền xong thì z bắt 
            // buộc phải thay đổi. Suy ra ở hàm trên bắt buộc phải có hàm để thay đổi giá trị của c.Ở đây chỉ truyền
            // địa chỉ chứ k có truyền giá trị.
            Console.WriteLine($"x={x}, y={y}, z={z}");
            // x=5,y=2,z=7
            //----------------------------------------
            //int[] arr = { x, y, z };
            //SumArray(out s, arr);
            SumArray(out s, x, y, z);
            Console.WriteLine(s);
            Console.ReadLine();
        }
    }
}
