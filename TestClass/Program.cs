﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClass
{
    class Program
    {
        static void Main(string[] args)
        { 
            Product p1 = new Product(1000, "Caphe");
            p1.PrintProduct();
            Console.ReadLine();
        }
    }

    public class Product
    {
        private int ID;
        private string productName;

        public Product()
        {
            ID = -1;
            productName = "no name";
        }

        public Product(int id, string ProductName)
        {
            this.ID = id;
            this.productName = ProductName;
        }

        public void PrintProduct()
        {
            Console.WriteLine($"ID={ID}, Name={productName}");
            // Cách viết {} tồn tại ở bản 6.0 thay cho việc sử dụng dấu %
        }
    }
}
