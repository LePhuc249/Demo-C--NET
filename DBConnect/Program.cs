﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration; 
using System.Data.SqlClient;
using System.Data.Common;
using static System.Console;

namespace DBConnect
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("********Fun with data provider factories********\n");
            string dp = ConfigurationManager.AppSettings["provider"];
            string cnStr = ConfigurationManager.ConnectionStrings["SqlProviderPubs"].ConnectionString;

            DbProviderFactory df = DbProviderFactories.GetFactory(dp);

            DbConnection cn = df.CreateConnection();
            WriteLine("Your connection Object is a: {0}", cn.GetType().FullName);
            cn.ConnectionString = cnStr;
            cn.Open();

            DbCommand cmd = df.CreateCommand();
            WriteLine("Your command Object is a: {0}", cmd.GetType().FullName);
            cmd.Connection = cn;
            cmd.CommandText = "Select * from Authors";

            DbDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            WriteLine("Your data render Object is a: {0}", dr.GetType().FullName);

            WriteLine("\n ******Author in Pubs******");
            while (dr.Read())
            {
                WriteLine("->{0}, {1}", dr["au_lname"], dr["au_fname"]);
            }
            dr.Close();
            ReadLine();
        }
    }
}
