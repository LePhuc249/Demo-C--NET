﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idemonation
{
    interface Idemonation
    {
        float getLength();
        float getWeigth();
    }

    class Boxes: Idemonation
    {
        private float length;
        private float weigth;

        public Boxes(float length, float weigth) => (this.length, this.weigth) = (length, weigth);

        public float getLength() => length;

        public float getWeigth() => weigth;

        public override string ToString() => $"Length ={length}, Weigth = {weigth}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            Boxes box = new Boxes(3.2f, 5.3f);
            Console.WriteLine(box);
            Idemonation demoension = new Boxes(5.6f, 2.4f);
            Console.WriteLine(demoension.getLength() + ", "+ demoension.getWeigth());
            Console.ReadLine();
        }
    }
}
